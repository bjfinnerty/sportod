import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../action_creators';

import NbaTeam from './NbaTeam';
import Torrents from './Torrents';


export const Featured = React.createClass({

	render: function() {
		return <div className='featured'>
			<div className='teams'>
				<NbaTeam team={this.props.game['home_team']} align='left '/>
				<div><span> VS </span></div>
				<NbaTeam team={this.props.game['away_team']} align='right '/>
			</div>
			<div className='torrents'>
				<Torrents gameId={this.props.game.game_id} torrents={this.props.game.torrents} {...this.props} ></Torrents>
			</div>
		</div>
	}

});

function mapStateToProps(state){

	return{
		'meta' : state.get('meta').toJS()
	}
}

export const FeaturedGame = connect(mapStateToProps, actionCreators)(Featured);