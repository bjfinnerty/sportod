var PirateBay = require('thepiratebay'),
	moment = require('moment'),
	_ = require('lodash');


PirateBay.default.baseUrl = 'https://ahoy.one';

var defaultOpts = {
	'category': 'video',
		'filter': {
		'verified': true    
	},
	'page': 0,
	'orderBy': 'size',
	'sortBy': 'desc'
}

var filterList = {
	//definition 	: ['720p', '1080p'],
	//frameRate 	: '60fps',
	size		: 2000000000,
	seeders		: 0,
}	


function getQueryString(game, teams){
	
	var league = 'nba';
	var gameYear = moment(game.game_date_est, 'YYYY-MM-DDT00:00:00').year();
	var homeTeamName = game.home_team_id.team_name;
	var visitorTeamName = game.visitor_team_id.team_name;

	return [league, gameYear, homeTeamName, visitorTeamName].join(' ');
}


function search(queryString, options){

	// for overwriting later on
	//var queryOpts = _.extend(option, defaultOpts)

	return PirateBay.search(queryString, defaultOpts)
	.then(function(results) {
	    return results;
	})
	.catch(function(err) {
	    return err;
	});
}

function filterResults(torrentList, date){

	var validTorrents = _.filter(torrentList, function(torrent){

		var uploadDate = moment(torrent.uploadDate, "MM-DD HH:mm");

		if(torrent.name.indexOf('720') === -1 && torrent.name.indexOf('1080') === -1){
			// Check for the definition in the title
			return false;
		}else if((uploadDate < moment(date)) || (uploadDate > moment(date).add(3, 'days'))){
			// If the torrent file was uploaded before the date the game was played or more than 3 days later, reject it. It is the wrong game
			return false;
		}else if(torrent.size.indexOf('GiB') === -1 || parseInt(torrent.size) < 3){
			// If the size is less than 3 gb we don't want it. The quality is poor
			return false;
		//}else if(torrent.seeders === 0 || (torrent.leechers > torrent.seeders)){
			// Make sure it can be downloaded
		//	return false;
		}else{
			return true;
		}
	});

	return validTorrents
}






module.exports = {
	'pirateBay' : PirateBay,
	'getQueryString' : getQueryString,
	'search': search,
	'filterResults': filterResults
}