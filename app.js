var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose'),
  socketUtils = require('./app/utils/socket.js'),
  http = require('http');

mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/app/*/models/*.js');

models.forEach(function (model) {
  require(model);
});

var app = express();
var server = http.Server(app);
var io = socketio(server);

require('./config/express')(app, config);

server.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});


io.on('connection', function (socket) {
	console.log(socket.id);

	socket.on('create', function(room) {
	    socket.join(room);
	    //socket.in(room).emit('action', {type:'SOCKET_MESSAGE', data:socket.id});
	    io.to(room).emit('action', {type:'SOCKET_MESSAGE', data:'timeout'});
	    module.exports.socket = socket;
	});
	
	
	//socket.broadcast.emit('action', {type:'SOCKET_MESSAGE', data:socket.id});
	
});

module.exports.io = io;

