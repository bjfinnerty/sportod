// var webpack = require('webpack');

// module.exports = {
// 	entry: [
// 		'webpack-dev-server/client?http://localhost:8080',
//     	'webpack/hot/only-dev-server',
// 		'./src/index.jsx'
// 	],
// 	module: {
// 		preLoaders: [
// 	        { 
// 	        	test: /\.json$/, exclude: /node_modules/, loader: 'json'
// 	        },
// 	    ],
// 	    loaders: [
// 	    	{ test: /\.json$/, exclude: /node_modules/, loader: 'json' },
// 	    	{ test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel' },
// 		    { test: /\.(svg|png|jpe?g|ttf|woff2?|eot)$/, loader: 'url?limit=8182' },
// 		    { test: /\.css$/, loader: "style-loader!css-loader" }
// 	    ]
// 	  },
// 	  resolve: {
// 	    extensions: ['', '.js', '.jsx', '.json', '.css','.svg'],
// 	    modulesDirectories: ['node_modules']
// 	  },
// 	output: {
// 		path : __dirname + '/dist',
// 		publicPath : '/',
// 		filename : 'bundle.js'
// 	},
// 	devServer: {
// 		contentBase : './dist'
// 	},
// 	plugins: [
// 		new webpack.HotModuleReplacementPlugin()
// 	]
// }

var webpack = require('webpack');

module.exports = {
  entry: [
     'webpack-dev-server/client?http://localhost:8080',
     'webpack/hot/only-dev-server',
    './src/index.jsx'
  ],
  module: {
    loaders: [
      {test: /\.jsx?$/, exclude: /node_modules/, loader: 'react-hot!babel'},
      { test: /\.css$/, loader: "style!css" },
      { test: /\.png$/, loader: "url-loader?limit=100000" },
      { test: /\.json$/, exclude: /node_modules/, loader: 'json' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
      { test: /\.(woff|woff2)$/, loader:"url?prefix=font/&limit=5000" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/dist',
    publicPath: './dist',
    filename: 'bundle.js'
  },
  devServer: {
  	publicPath: 'http://localhost:8080/',
    contentBase: 'http://localhost:8080/dist',
    hot: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};