var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

// Torrents
var KatTorrentSchema = new Schema({
	_id: String,
	title: String,
	category: String,
	link: String,
	guid: {type : Schema.Types.ObjectId, unique: true, dropDups: true},
	pubDate: String,
	torrentLink: String,
	files: Number,
	comments: Number,
	hash: String,
	peers: Number,
	seeds: Number,
	leechs: Number,
	size: Number,
	votes: Number,
	verified: Number,
	game_id: { type: String, ref: 'Game' }
});

var PBTorrentSchema = new Schema({
	_id: Number,
	name: String,
	link: String,
	guid: {type : Schema.Types.ObjectId, unique: true, dropDups: true},
	uploadDate: String,
	magnetLink: String,
	seeders: Number,
	leechers: Number,
	size: String,
	verified: Boolean,
	game_id: { type: String, ref: 'Game' }
});

PBTorrentSchema.pre('remove',  function(next){
	var gameModel = this.model('Game');
	console.log(gameModel);
	next();
});

mongoose.model('Torrent', PBTorrentSchema);

