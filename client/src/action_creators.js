import moment from 'moment';
import io from 'socket.io-client';
import {socket} from './index.jsx';

export function loadTodaysGames(date=new Date()){
    
    const mom = moment(date).format('MM/DD/YYYY');
    return function(dispatch, getState){
        
        fetch('/nba/games?date=' + mom, {
            'mode': 'no-cors',
            'redirect': 'follow',
            'headers': new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(response => {
            return response.json();
        })
        .then(gameData => {
            return dispatch(updateGameState(gameData));
        })
        .catch(err => {
            console.log(err)
        });

    }
}


export function refreshGames(date = new Date()){

    const mom = moment(date).format('MM/DD/YYYY');

    return function(dispatch, getState){
        
        fetch('/nba/games/refresh?date=' + mom, {
            'mode': 'no-cors',
            'redirect': 'follow',
            'headers': new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(response => {
            return response.json();
        })
        .then(gameData => {
            return dispatch(updateGameState(gameData));
        })
        .catch(err => {
            console.log(err)
        });

    }
}

export function updateDateState(mmnt){
    return {
        'type': 'UPDATE_DATE',
        'dateString': mmnt.format('YYYY-MM-DDTHH:mm:ss')
    }
}

export function updateTorrentState(gameId, torrents){

	return {
		'type' : 'UPDATE_TORRENT_DATA',
		'gameId' : gameId,
        'torrents': torrents
  }
}

export function updateGameState(gameData){

    return {
        'type' : 'UPDATE_GAME_DATA',
        'games' : gameData.games,
        'meta' : gameData.meta
    }
}

export function changeFeatured(featuredIndex){

    return {
        'type' : 'UPDATE_FEATURED_GAME',
        'featuredIndex' : featuredIndex
    }
}

export function updateActiveDownloads(gameId, data){

    return {
        'type' : 'UPDATE_ACTIVE_DOWNLOADS',
        'data': data
    }
}

export function getTorrentData(gameId){

 	return function(dispatch, getState){

        fetch('/nba/games/search-torrent/'+gameId + '/', {
            'mode': 'no-cors',
            'redirect': 'follow',
            'headers': new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(response => {
            return response.json();
        })
        .then(torrents => {
            return dispatch(updateTorrentState(gameId, torrents));
        })
        .catch(err => {
            console.log(err)
        });

	};
}

export function startDownload(torrentLink, gameId){
    
    return function(dispatch, getState){

        fetch(`/nba/games/${gameId}/torrents/download/`,{
            'method': 'post',
            'mode': 'cors',
            'redirect': 'follow',
            'headers': new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({
                'gameId' : gameId,
                'torrentLink': torrentLink
            })
        })
        .then(response => {
            return response.json();
        })
        .then(torrents => {

            // socket.on('problem', (data) => {
            //     console.log('problem', data); 
            // });

            // This theoretically could create a race condition with data coming back from the socket 
            // but in reality it should never be a problem
            const initialData = {
                'status' : 'fetching metadata',
                'torrentLink': torrentLink,
                'gameId' : gameId,
                'percentDown': 0,
                'downloadSpeed': 0,
                'progress': 0,
                'totalSize': 0,
                'peers' : 0
            }
            return dispatch(updateActiveDownloads(gameId, initialData));

        })
        .catch(err => {
            console.log(err)
        });
    }


    // return {
    //     type : 'START_TORRENT_DOWNLOAD',
    //     torrentLink : torrentLink,
    //     meta: {
    //         openSocket: true
    //     }
    // }
}