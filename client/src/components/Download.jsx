import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import * as actionCreators from '../action_creators';

export const Download = React.createClass({

	render: function() {

		return <div className='downloading'>
			<ul className='dynamicData'>
				<li>Size : {this.props.data.totalSize}</li>
				<li>Progress : {this.props.data.progress}</li>
				<li>Speed : {this.props.data.downloadSpeed}</li>
			</ul>
			<ul>
				<li><a href='#'>Pause</a></li>
				<li><a ahref='#'>Abort</a></li>
				<li><a href='#'>Play</a></li>
			</ul>
		</div>
	}

});

