var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var ScoreSchema = new Schema({
	"game_date_est": String,
	"game_sequence": Number,
	"game_id": {'type': String, 'ref': 'Game'},
	"team_id": {'type' : Number, 'ref': 'Team'},
	"team_abbreviation": String,
	"team_city_name": String,
	"team_wins_losses": String,
	"pts_qtr1": Number,
	"pts_qtr2": Number,
	"pts_qtr3": Number,
	"pts_qtr4": Number,
	"pts_ot1": Number,
	"pts_ot2": Number,
	"pts_ot3": Number,
	"pts_ot4": Number,
	"pts_ot5": Number,
	"pts_ot6": Number,
	"pts_ot7": Number,
	"pts_ot8": Number,
	"pts_ot9": Number,
	"pts_ot10": Number,
	"pts": Number,
	"fg_pct": Number,
	"ft_pct": Number,
	"fg3_pct": Number,
	"ast": Number,
	"reb": Number,
	"tov": Number
});

mongoose.model('Score', ScoreSchema);