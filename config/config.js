var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'sporttv'
    },
    port: 3000,
    db: 'mongodb://localhost/sporttv-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'sporttv'
    },
    port: 3000,
    db: 'mongodb://localhost/sporttv-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'sporttv'
    },
    port: 3000,
    db: 'mongodb://localhost/sporttv-production'
  }
};

module.exports = config[env];
