import DatePicker from 'react-datepicker';
import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {AllActiveDownloads} from './ActiveDownloads';
import {connect} from 'react-redux';
import * as actionCreators from '../action_creators';
import moment from 'moment';
require('react-datepicker/dist/react-datepicker.css');

export const HeaderCmp = React.createClass({

	getDate: function(){

		if(this.props.meta.date){
			return new moment(this.props.meta.date);
		}else{
			return new moment().subtract(2, 'days');
		}
	},

	changeDate:function(mmnt){
		return this.props.updateDateState(mmnt);
	},

	render: function() {
		return <div>
			<h3>Header</h3>
			<DatePicker selected={this.getDate()} onChange={this.changeDate} />
			<AllActiveDownloads />
		</div>
	}

});

function mapStateToProps(state){
	return{
		'meta' : state.get('meta').toJS()
	}
}

export const Header = connect(mapStateToProps, actionCreators)(HeaderCmp);