var path        = require("path"),
    fs          = require('fs'),
    WebTorrent  = require('webtorrent'),
    filePath    = path.join(__dirname, "../torrents/");

var saveProgress = function(torrent, progress, chunkSize){

    // console.log('chunk size: ' + chunkSize);
    // console.log('torrent pieces: ' + torrent.pieces.length);
    // console.log('total downloaded: ' + torrent.downloaded);
    // console.log('download speed: ' + torrent.downloadSpeed());
    // console.log('progress: ' + torrent.progress);
    // console.log('total size: ' + torrent.length);
    // console.log('======')

    console.log('torrent progress: ' + progress);
    
};

var startDownload = function(torrentLink){

    var client = new WebTorrent();

    return client.add(torrentLink, {'path':filePath}, function (torrent) {
        console.log('torrent link has been added to client');
        return torrent;
    });   
};

var stopDownload = function(torrent, callback){

    var defaultCallback = function(){
        console.log(arguments);
        return false;
    };
    var cb = callback || defaultCallback;

    return torrent.destroy(torrentId, cb);
}

module.exports = {
    'startDownload': startDownload,
    'stopDownload' : stopDownload
};

