var path 		= require("path"),
	fs 			= require('fs'),
    WebTorrent  = require('webtorrent');


var raptorsCavs = "https://torcache.net/torrent/D353EB6F8F821B3BB0D5B0D15306B3D1DE6736FC.torrent?title=[kat.cr]nba.2015.rs.25.nov.cle.cavaliers.v.tor.raptors.720p.jaguar777";
var pistonsWizards = "https://torcache.net/torrent/70BB41A65D10E7B3E3F5E35A5B0EB717BBF58A93.torrent?title=[kat.cr]nba.21.11.2015.rs.wizards.pistons.home.feed.h264.mkv.30fps.aac.720p.mr.drax"
var beatles = "https://torcache.net/torrent/5DABB034B473B906738F754C5CD03AEAACECEE22.torrent?title=[kat.cr]the.beatles.help";
var cavsHeat = "magnet:?xt=urn:btih:5C738AF0576373A767136075C92B4EE77AB36DFE&dn=nba+2015+12+05+rs+cavaliers+heat+720p+webrip+60fps+h264+hrd+hariniz&tr=udp%3A%2F%2Ftracker.publicbt.com%2Fannounce&tr=udp%3A%2F%2Fglotorrents.pw%3A6969%2Fannounce"


var client = new WebTorrent();
var filePath = path.join(__dirname, "../torrents/");


var saveProgress = function(torrent){

	
};

client.add(cavsHeat, {path:filePath}, function (torrent) {

	var progress = 0;

	torrent.on('download', function(chunkSize){

		// var perCentDown = Math.round(torrent.progress * 100);
		
		// if(progress < perCentDown){
		// 	progress = perCentDown;
		// 	saveProgress();
		// }

		// if(progress === 100){
		// 	// Finish torrent and close client
		// }

		console.log('chunk size: ' + chunkSize);
		console.log('torrent pieces: ' + torrent.pieces.length);
		console.log('total downloaded: ' + torrent.downloaded);
		console.log('download speed: ' + torrent.downloadSpeed());
		console.log('progress: ' + torrent.progress);
		console.log('total size: ' + torrent.length);
		console.log('======');
	});

    // Got torrent metadata! 
    console.log('Client is downloading:', torrent.infoHash);
    
});   