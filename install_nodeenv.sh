#!/usr/bin/env bash

virtualenv --distribute env;
source env/bin/activate;
pip install nodeenv;
nodeenv -p --node=5.3.0 --prebuilt;
npm install;