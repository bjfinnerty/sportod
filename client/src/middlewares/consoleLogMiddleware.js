import {fromJS} from 'immutable';


export default consoleLog => store => next => action => {
	
	console.log('before');
	const returnVal =  next(action);
	console.log('after'); 
	return returnVal;
}

