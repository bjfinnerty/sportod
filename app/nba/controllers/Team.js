
// Teams
var getTeam = function(opts){

    var opts = opts || {};
    _.extend(query_params, opts);

    return Team.find(query_params).exec();

};

var teamIndex = function (req, res, next) {


    if(req.query.short_code){
        query_params.team_abbreviation = req.query.short_code;
    }

    getTeam(query_params)
    .then(function(teams){
        return res.json(teams);
    })
    .catch(function(err){
        return next(err);
    });

};


var refreshTeam = function(req, res, next){

    if(req.query.short_code){
        query_params.team_abbreviation = req.query.short_code;
    }

    getTeam(query_params)
    .then(function(teams){
        teams.forEach(function(team, i){
            team.refresh();
            console.log(team.team_name, team.team_code);

            //return res.json({team:team.refresh()});
        });
    })
    .catch(function(err){
        return next(err);
    });

    return res.json([]);

};

var getTeamById = function(){

}

var getTeamsByIds = function(ids){
    
    return Team.find({
        '_id': { $in: ids }
    }).then(function(teams){
        return teams;
    })
    .catch(function(err){
        return next(err);
    });
}
