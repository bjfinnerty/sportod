import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {connect} from 'react-redux';
import {Download} from "./Download";
import * as actionCreators from '../action_creators';


export const ActiveDownloads = React.createClass({

	render: function() {

		let downloads = this.props.downloads.map( (data, idx) => {
			return <Download key={data.torrentId} data={data} />;
		});

		return <div id='activeDownloads'>
			<h4>
				Downloads: ( {this.props.downloadCount} )
			</h4>
			{downloads}
		</div>
	},

	renderTorrent: function(){

	}

});

function mapStateToProps(state){

	return{
		'downloadCount' : state.get('downloads').size,
		'downloads' : state.get('downloads').toArray()
	}
}

export const AllActiveDownloads = connect(mapStateToProps, actionCreators)(ActiveDownloads);