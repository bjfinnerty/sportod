import React from 'react';
import ReactDOM from 'react-dom';
import Router, {Route} from 'react-router';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {List, Map} from 'immutable';
import reducer from './reducer';
import App from './components/App';
import { NbaGame } from './components/NbaGame';
import {Header} from './components/Header';
import moment from 'moment';

require('./styles/styles.css');

// Create Socket and apply as middleware
import createSocketIoMiddleware from 'redux-socket.io';
import io from 'socket.io-client';
const socket = io.connect('http://localhost:3000');
socket.emit('create', 'nba');

// socket.on('create', function (room) {
//     console.log(room);
//     socket.join('nba');
// });

const socketIoMiddleware = createSocketIoMiddleware(socket);



// create a store that has redux-thunk, and socket middleware enabled
const createStoreWithMiddleware = applyMiddleware(thunk, socketIoMiddleware)(createStore);
const store = createStoreWithMiddleware(reducer);

const initialState = Map({
    'games':List(),
    'meta': Map({
        'date': moment("20160619", "YYYYMMDD").format('YYYY-MM-DDTHH:mm:ss'),//new moment().subtract(1, 'days').format('YYYY-MM-DDTHH:mm:ss'),
        'featuredIndex': 0,
        'loaded':false
    }),
    'downloads':Map()
});

store.dispatch({
    'type': 'SET_STATE',
    'state': initialState
});


ReactDOM.render(
    <Provider store={store}> 
        <Header />
    </Provider>,
    document.getElementById('header')
);

ReactDOM.render(
    <Provider store={store}> 
  	    <NbaGame />
    </Provider>,
  	document.getElementById('app')
);

