socketio = require('socket.io');

function openSocket(port){

    var socketPort = port || 3001;

    var io = socketio();
    io.on('connection', function(socket){
        console.log('Socket opened');
    });

    io.listen(port);

    return io;
}

function closeSocket(socket){
    socket.emit('close');
    socket.close();
    console.log('Socket Closed');

}

module.exports = {
    'openSocket': openSocket,
    'closeSocket' : closeSocket
};