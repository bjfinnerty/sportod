var request = require('request-promise'),
    Promise = require('bluebird'),
    _ = require('lodash'),
    moment = require('moment');

// NBA Season Constants
var scoreBoardUrl   = 'http://stats.nba.com/stats/scoreboardV2',
    teamUrlRoot     = 'http://stats.nba.com/stats/teaminfoCommon'  
    leagueId        = '00',
    season          = '2015-16',
    seasonType      = 'Regular Season';
 


var nbaReq = request.defaults({
    headers: {
        'content-type': 'application/json',
        'host': 'stats.nba.com',
        'Referer': 'http://stats.nba.com/scores/'
    }
});

var getGamesOnDate = function(date){

    var queryOptions = {
        dayOffset: 0,
        gameDate: date,
        leagueId: leagueId
    };

    return nbaReq.get({
        uri: scoreBoardUrl,
        qs : queryOptions}
    );

};


var getTeamsData = function(teamId){
    var queryOptions = {
        season: season,
        seasonType: seasonType,
        teamId: teamId,
        leagueId: leagueId
    };

    return nbaReq.get({
        uri: teamUrlRoot,
        qs : queryOptions
    });
}


var parseData = Promise.method(function (data, resultSetIndex) {

    resultSetIndex = resultSetIndex || 0;
    var jsonData = JSON.parse(data);
    var results = jsonData.resultSets[resultSetIndex].rowSet;
    var headers = jsonData.resultSets[resultSetIndex].headers;
    var returnData = [];

    results.forEach(function(result, i){
        var resultObj = {};
        result.forEach(function(dataPoint, i) {
            resultObj[headers[i].toLowerCase()] = dataPoint;
        });
        returnData.push(resultObj);
    });

    return returnData;
});


var addScoresToGame = Promise.method(function(games, scores){

    games.forEach(function(game, i){

        var homeTeamStats = _.findWhere(scores, {'team_id' : game.home_team_id});
        var awayTeamStats = _.findWhere(scores, {'team_id' : game.visitor_team_id});

        game.home_team = homeTeamStats;
        game.away_team = awayTeamStats;
        game.score_diff = Math.abs(homeTeamStats.pts - awayTeamStats.pts);
        game.score_agg = homeTeamStats.pts + awayTeamStats.pts;

    });

    return games;

});

var nowString = function(){
    return moment().format('YYYY-MM-DDTHH:mm:ss');
}


module.exports = {
	nbaReq : nbaReq,
	parseData : parseData,
    getTeamsData: getTeamsData,
    getGamesOnDate: getGamesOnDate,
    addScoresToGame : addScoresToGame,
    nowString: nowString
}



