var http_mocks = require('node-mocks-http'),
    should = require('should'),
    mockery = require('mockery')
    mongoose = require('mongoose'),
    app = require('../app.js'),
    router = require('../app/nba/routes.js');

// var models = glob.sync(config.root + '/app/*/models/*.js');

// models.forEach(function (model) {
//   require(model);
// });

function buildResponse() {
    return http_mocks.createResponse({eventEmitter: require('events').EventEmitter})
}


describe('Welcome Controller Tests', function() {

    before(function() {
        // mockery.enable({
        //     'warnOnUnregistered': false
        // })

        // mockery.registerMock('../app/nba/models/Team', {
        //     all: (cb) => cb(null, ["First news", "Second news"]),
        //     create: (title, text, cb) => cb(null, {title:title, text:text, id: Math.random()})
        // })

        //this.controller = require('../app/nba/controllers.js');
    })

    after(function() {
        mockery.disable()
    })

    it('hello', function(done) {
        var response = buildResponse()
        var request  = http_mocks.createRequest({
          method: 'GET',
          url: '/nba/test',
        })

        response.on('end', function() {
          response._getData().should.equal('world');
          done();
        })

        router.handle(request, response);
    })

//   it('hello fail', function(done) {
//     var response = buildResponse()
//     var request  = http_mocks.createRequest({
//       method: 'POST',
//       url: '/nba/testThis',
//     })

//     response.on('end', function() {
//       // POST method should not exist.
//       // This part of the code should never execute.
//       done(new Error("Received a response"))
//     })

//     controller.handle(request, response, function() {
//       done()
//     })
//   })
})