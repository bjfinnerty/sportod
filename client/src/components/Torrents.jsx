import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getTorrentData } from '../action_creators';

// export const TorrentCmp = React.createClass({

export default React.createClass({

	mixins : [PureRenderMixin],

	getTorrents: function(e, gameId){
		e.preventDefault();
		return this.props.getTorrentData ? this.props.getTorrentData(gameId) : null;
	},

	startDownload: function(e, torrentLink, gameId){
		e.preventDefault();
		return this.props.startDownload ? this.props.startDownload(torrentLink, gameId) : null;
	},

	listTorrents: function(gameId) {
		return <ul>
			{
				this.props.torrents.map( torrent =>
					<li key={torrent.hash} className='torrent'>
						<a href='#' onClick={(e) => this.startDownload(e, torrent.torrentLink, gameId)}>
							{torrent.name}
						</a>
					</li>
				)
			}
		</ul>
	},

	render: function() {
		return <div>
			{
					this.listTorrents(this.props.gameId)
			}
			<a href='#' className='torrent-search' onClick={(e) => this.getTorrents(e, this.props.gameId)}>Update Torrent Data</a>

		</div>;
	}

});
