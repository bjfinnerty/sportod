var express = require('express'),
  router = express.Router(),
  moment = require('moment'),
  controller = require('./controllers.js');


// Middleware
function requiredDate(req, res, next){

    var date = req.query.date;
    if(!date) {
        date = moment().subtract(1, 'days').format('MM/DD/YYYY');
        return res.redirect('?date=' + date);
    }
    else{
        next();
    }
}  

// Test controller
router.get('/test', controller.testThis);

// Teams routes
router.get('/teams', controller.teamIndex);
router.put('/teams/refresh', controller.refreshTeam);

// Games routes
router.get('/games/search-torrent/:game_id', controller.updateTorrents);

// Posting torrent link as it can be pretty big
router.post('/games/:game_id/torrents/download/', controller.downloadTorrent);


// Middleware here for setting dates
router.use(requiredDate);

router.get('/games', controller.gameIndex);
router.get('/games/refresh', controller.refreshGames);


module.exports = router;
module.exports.urlRoot = '/nba';