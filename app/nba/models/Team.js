var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  moment = require('moment'),
  utils = require('../utils.js');

// Constants
var teamUrlRoot = 'http://stats.nba.com/stats/teaminfoCommon';
var season = '2015-16';
var seasonType = 'Regular Season';

var TeamSchema = new Schema({
	_id: Number,
	sport: String,
	league: String,
	league_id: String,
	season_year: String,
	team_city: String,
	team_name: {type : String, unique: true, dropDups: true},
	team_abbreviation: {type : String, unique: true, dropDups: true},
	team_conference: String,
	team_division: String,
	team_code: {type : String, unique: true, dropDups: true},
	w: Number,
	l: Number,
	pct: Number,
	conf_rank: Number,
	div_rank: Number,
	min_year: String,
	max_year: String,
	last_updated: { type : Date, default: Date.now }
});

TeamSchema.methods = {

  refresh: function () {

	  var self = this;
	  this.fetchRemote()
		  .then(function(data){
			  var teamData  = utils.parseData(data)[0];
			  self.update(teamData, function(err, mResp){
				  if(err){
					  return err;
				  } else {
					  return mResp;
				  }
			  });

		  })
		  .catch(function(){
			  console.log('error');
		  });

	},

	fetchRemote: function(){
		var queryOptions = {
			season: season,
			seasonType: this.seasonType || seasonType,
			teamId: this._id,
			leagueId: this.league_id
		};

		return utils.nbaReq.get({
			uri: teamUrlRoot,
			qs : queryOptions}
		);
	}
};

mongoose.model('Team', TeamSchema);
