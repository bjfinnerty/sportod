var express = require('express');
var router = express.Router();
var models  = require('../models');

/* GET users listing. */
router.get('/', function(req, res, next) {

  models.User.findAll().then(function(users) {

    res.render('index', {
      title: 'Express',
      users: users
    });
  });
});

//   res.send('respond with a resource');
// });

module.exports = router;
