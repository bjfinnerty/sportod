import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';

export default React.createClass({

	mixins : [PureRenderMixin],

	getTeam: function() {
	    return this.props.team || [];
	},

	getAlign: function() {
	    return this.props.align || [];
	},

	render: function() {

		var team = this.getTeam();
		var align = this.getAlign();

		return <div className={"team align-"+ align }>
			{
				<div>
					<span className='team-name'>{team.team_abbreviation}</span>
					<figure className='logo'>
						<img src={'/img/' + team.team_abbreviation +'_logo.svg'} />
					</figure>
				</div>
			}
		</div>;
	}

});