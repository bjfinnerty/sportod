var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Team = mongoose.model('Team'),
    Game = mongoose.model('Game'),
    Torrent = mongoose.model('Torrent'),
    Meta = mongoose.model('Metadata'),
    Score = mongoose.model('Score'),
    moment = require('moment'),
    utils = require('./utils.js'),
    _ = require('lodash')
    kat = require('./kat.js'),
    TorrentManager = require('../utils/pirate.js'),
    socketUtils = require('../utils/socket.js'),
    torrentUtils = require('../utils/torrents.js'),
    mongoose.Promise = require('bluebird'),
    socketio = require('socket.io'),
    app = require('../../app.js');

var query_params = {'league': 'nba'};


// Teams
var getTeam = function(opts){

    var opts = opts || {};
    _.extend(query_params, opts);

    return Team.find(query_params).exec();

};

var teamIndex = function (req, res, next) {

    if(req.query.short_code){
        query_params.team_abbreviation = req.query.short_code;
    }

    getTeam(query_params)
    .then(function(teams){
        return res.json(teams);
    })
    .catch(function(err){
        return next(err);
    });

};


var refreshTeam = function(req, res, next){

    if(req.query.short_code){
        query_params.team_abbreviation = req.query.short_code;
    }

    getTeam(query_params)
    .then(function(teams){
        teams.forEach(function(team, i){
            team.refresh();
            console.log(team.team_name, team.team_code);

            //return res.json({team:team.refresh()});
        });
    })
    .catch(function(err){
        return next(err);
    });

    return res.json([]);

};

var getTeamById = function(){

}

var getTeamsByIds = function(ids){
    
    return Team.find({
        '_id': { $in: ids }
    }).then(function(teams){
        return teams;
    })
    .catch(function(err){
        return next(err);
    });
}

// Games 

var gameIndex = function (req, res, next) {

    // format the date parameter to a string for easier querying of the db 
    var dateMmt = moment(req.query.date, 'MM/DD/YYYY');
    var dateStr = dateMmt.format('YYYY-MM-DDT00:00:00');

    // Get a list of the games from the database for the day as a promise
    var games = Game.find({
        'game_date_est': dateStr
    })
    .populate('torrents')
    .sort({
        'live_period': 'desc',
        'score_diff' : 'asc',
        'score_agg' : 'desc'
    })
    .exec();

    // Get the metadata from the database for the games collection as a promise
    var meta = Meta.findOne({'type': 'game'}).exec();
    
    // When all promises have been resolved return the data as json
    Promise.all([games, meta])
        .then(function(results){
            var games = results[0];
            var meta = results[1];
            return res.json({ 
                'games':games, 
                'meta' : { 
                    'updated': meta.updated, 
                    'date':dateMmt.toDate() 
                } 
            });
        })
        .catch(function(err){
            return next(err);
        });
};

var getGameById = function(id){

    // Get game by game id

    return Game.findOne({'game_id': id})
        .populate('home_team_id')
        .then(function(gameModel){
            if(gameModel){
                return gameModel;
            }else{
                throw({'status': 404, 'message':'No Games for the id:' + id });
            }
        })
        .catch(function(err){
            return next(err);
        });
}

var refreshGames = function(req, res, next){

    // Get games from nba api
    // Make a request to the api returning a promise

    utils.getGamesOnDate(req.query.date)
        .then(function(data){

            // Parse the reponse body to get game data and scores
            // Returns a promise
            var games = utils.parseData(data, 0);
            var scores = utils.parseData(data, 1);

            return Promise.all([games, scores]);
        })
        .then(function(gameData){
            
            if(!gameData[0].length){
                return Promise.reject('No Games for this day');
            }
            // Merge score data to game data. Returns a promise
            return utils.addScoresToGame(gameData[0], gameData[1]);
        })
        .then(function(scoredGames){ 
            // Store the game data to the database. Returns a promise
            return storeGameData(scoredGames);
        }).
        then(function(mResp){
            // After success, redirect to the index page so the API returns everything in a standard way
            res.redirect('/nba/games?date='+ req.query.date )
        })
        .catch(function(err){
            if(err === 'No Games for this day'){
                res.redirect('/nba/games?date='+ req.query.date );
            }else{
                next(err);
            }
        });
};


var storeGameData = function(scoredGames){

    // Ininitalise a bulk operation
    var updateGames = Game.collection.initializeOrderedBulkOp();

    // Loop through the games and search for an existing game in the database
    // If it is not there insert and if it is update
    scoredGames.forEach(function(game, i){
        updateGames.find({game_id:game.game_id}).upsert().updateOne( game );
    });

    // create the metadata object and function to execute if the update is successful
    var metaData = {updated: utils.nowString()};
    var updateMeta =  Meta.update({type: 'game'}, metaData, {upsert: true});

    return Promise.all([updateGames.execute(), updateMeta.exec()]);
};


// Torrents

var updateTorrents = function(req, res, next){

    var gameId = req.params.game_id;
    var game;

    Game.findOne({
        'game_id': gameId
    })
    .populate(['home_team_id', 'visitor_team_id'])
    .then(function(gameModel){
        // Make game available in controller scope
        game = gameModel;
        return game.removeTorrents();
    })
    .then(function(){
        var searchTerms = TorrentManager.getQueryString(game);
        return TorrentManager.search(searchTerms, {});
    })
    .then(function(torrents){

        // filter torrents based on meta data
        var validTorrents = TorrentManager.filterResults(torrents, game.game_date_est);

        // save game model to db and save torrents to db 
        setTimeout(function(){
            game.saveTorrents(validTorrents);
        },1);

        return res.json(validTorrents);
    })
    .catch(function(err){
        return next(err);
    });
}


var downloadTorrent = function(req, res, next){
        
    // get the link from the body and the game id from the url
    var torrentLink = req.body.torrentLink;
    var gameId = req.params.game_id;

    var torrent = torrentUtils.startDownload(torrentLink);
    var progress = 0;
    var socket = app.socket;
    var io = app.io;

    setTimeout(function(){
        if(!torrent.ready){
            socket.emit('problem', {
                'ready' : torrent.ready,
                'peers': torrent.numPeers
            });
        }
    }, 1000 * 10);

    //torrent.on('noPeers', function (announceType) {})
    //torrent.on('ready', function () {})
    // torrent.on('error', function (err) {})

    torrent.on('download', function(chunkSize){

        // Round current progress to nearest full integer so we only save progress at most 100 times per download
        var perCentDown = Math.round((torrent.progress || 0) * 100);
        console.log(torrent.progress, socket.id);

        io.to('nba').emit('action', {
            'type': 'SOCKET_TORRENT_CHUNK',
            'data':{
                'status' : 'downloading', 
                'torrentLink' : torrentLink,
                'gameId' : gameId,
                'percentDown': perCentDown,
                'downloadSpeed': torrent.downloadSpeed(),
                'progress': torrent.progress,
                'totalSize': torrent.length,
                'peers' : torrent.numPeers
            }
        });
        
        if(progress < perCentDown){
            progress = perCentDown;
            saveTorrentProgress(gameId, torrentLink, progress);
        }
    });

    torrent.on('done', function(){
        console.log('torrent finished downloading');
        socket.emit('action', {
            'type': 'SOCKET_TORRENT_ENDED',
            'data':{
                'status' : 'success', 
                'torrentLink' : torrentLink,
                'gameId' : gameId,
                'percentDown': perCentDown,
                'progress': torrent.progress,
                'totalSize': torrent.length,
            }
        });
        torrent.destroy();
    });

    return res.json({'download': 'torrentData'});
}

var saveTorrentProgress = function(gameId, torrentLink, progress){
    console.log('saveTorrent');
}




var testThis = function(req, res, next){

    utils.getGamesOnDate('06/19/2016')
        .then(function(data){

            // Parse the reponse body to get game data and scores
            // Returns a promise
            var games = utils.parseData(data, 0);
            var scores = utils.parseData(data, 1);

            return Promise.all([games, scores]);
        })
        .then(function(gameData){
            
            debugger;
            game = new Game(gameData[0][0]);
            //return game.saveScores(gameData[1]);
            return Game.find({'game_id':game.game_id}).update(gameData[0][0]);

            // Merge score data to game data. Returns a promise
            return utils.addScoresToGame(gameData[0], gameData[1]);
        })
        .then(function(scoredGames){ 
            // Store the game data to the database. Returns a promise
            return storeGameData(scoredGames);
        }).
        then(function(mResp){
            // After success, redirect to the index page so the API returns everything in a standard way
            res.redirect('/nba/games?date='+ req.query.date )
        })
        .catch(function(err){
            if(err === 'No Games for this day'){
                res.redirect('/nba/games?date='+ req.query.date );
            }else{
                next(err);
            }
        });

    //res.send('hello');
}



module.exports = {
  teamIndex: teamIndex,
  refreshTeam: refreshTeam,
  gameIndex: gameIndex,
  refreshGames: refreshGames,
  downloadTorrent: downloadTorrent,
  updateTorrents:updateTorrents,
  testThis: testThis
};
