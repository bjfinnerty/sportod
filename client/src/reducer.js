import {Map, List, fromJS} from 'immutable';

function setState(state, newState){
	const nextState = state.merge(newState);
	return nextState;
}

function getTorrentData(games, gameId){

	
	const targetIndex = games.findIndex(game => game.get('game_id') === gameId);
	
	// Make call to server to get torrent data
	const freshTorrents = require('./torrents.js').torrents;

	const newGames = games.update(targetIndex, (game) => game.set('torrents', freshTorrents));
	return newGames;
}

function addTorrentsToGame(state, gameId, torrents){

	const games = state.get('games');
	const targetIndex = games.findIndex(game => game.get('game_id') === gameId);
	const newGames = games.update(targetIndex, (game) => game.set('torrents', torrents));
	const newState = state.set('games', newGames);
	return newState;
}

const updateActiveDownloads = (state, torrentData) => {
	const torrentId = torrentData.torrentLink;
	return state.setIn(['downloads', torrentId], torrentData);
}

const updateGameData = (state, games, meta) => {

	const metaMap = fromJS(meta).set('loaded', true);
	const gamesList = fromJS(games);

	const updatedMeta = state.get('meta').merge(metaMap)

	return state.merge({
		'games': gamesList,
		'meta' : updatedMeta,
	});
}

const updateDate = (state, date) => {

	const gamesList = List([]);
	const metaMap = state.get('meta').merge({
		'date': date,
		'loaded': false
	});

	return state.merge({
		'games': gamesList,
		'meta' : metaMap,
	});
}

const updateFeaturedGame = (state, featuredIndex) => {
	return state.setIn(['meta', 'featuredIndex'], featuredIndex)
}

const startTorrenting = (state, torrentLink) => {
	return state;
}


export default function(state = Map(), action) {

	console.log(action.type,action.data);

	switch(action.type){
		case 'SET_STATE':
			return setState(state, action.state)

		case 'GET_TORRENT_DATA':
			const games = state.get('games');
			const newGames = getTorrentData(games, action.gameId);
			return state.set('games', newGames);

		case 'UPDATE_TORRENT_DATA':
			return addTorrentsToGame(state, action.gameId, action.torrents);

		case 'START_TORRENT_DOWNLOAD':
			return startTorrenting(state, action.torrentLink);

		case 'UPDATE_GAME_DATA':
			return updateGameData(state, action.games, action.meta);

		case 'UPDATE_DATE':
			return updateDate(state, action.dateString);

		case 'UPDATE_FEATURED_GAME':
			return updateFeaturedGame(state, action.featuredIndex);
			
		case 'SOCKET_TORRENT_CHUNK':
			return updateActiveDownloads(state, action.data); 

		case 'UPDATE_ACTIVE_DOWNLOADS':
			return updateActiveDownloads(state, action.data);

		case 'SOCKET_TORRENT_ENDED':
			console.log('===', action.data);
			return state
	}

  	return state;
}