var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
  
// Metadata
var MetadataSchema = new Schema({

	type: {type : String, unique: true, dropDups: true},
	updated : Date

});

mongoose.model('Metadata', MetadataSchema);
