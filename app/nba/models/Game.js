var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	_ = require('lodash');

// Games
var gameUrlRoot = 'http://stats.nba.com/stats/scoreboardV2';

var GameSchema = new Schema({
		'game_date_est': String,
		'game_id': {'type' : String, 'unique': true, 'dropDups': true},
		'game_status_id': Number,
		'game_status_text': String,
		'gamecode': {'type' : String, 'unique': true, 'dropDups': true},
		'home_team_id': {'type' : Number, 'ref': 'Team' },
		'home_team_stats': {'type' : Number, 'ref': 'Score'},
		'visitor_team_id': {'type' : Number, 'ref': 'Team' },
		'visitor_team_stats' : {'type' : Number, 'ref': 'Score'},
		'season' : String,
		'league_id' : {'type': String, 'default':'00'},
		'last_updated': 'Date',
		'torrents' : [{ 'type': Number, 'ref': 'Torrent' }],
		'live_period': Number,
		'score_diff': Number,
		'score_agg': Number
	},
	{ 
		'_id': false 
	}
);

GameSchema.methods = {

	saveScores : function(scoresArray){
		
		var bulkOp = this.model('Score').collection.initializeOrderedBulkOp();
		
		scoresArray.forEach(function(score, i){
	        bulkOp.find({'game_id':score.game_id, 'team_id':score.team_id}).upsert().updateOne( score );
	    });

	    if(bulkOp.length){
	        return bulkOp.execute();
	    }else{
	        return Promise.resolve([]);
	    }
	},

	removeTorrents:function(){
		var torrentModel = this.model('Torrent');
		return torrentModel.remove({
			'_id': { $in: this.torrents }
		});
	},

	saveTorrents : function(torrents){

		var bulkOp = this.model('Torrent').collection.initializeOrderedBulkOp();
		var gameId = this.game_id;

	    torrents.forEach(function(torrent, i){
	        var indexedTorrent = _.extend(torrent, {'game_id':gameId, '_id':torrent.id});
	        bulkOp.find({'_id':torrent.id}).upsert().updateOne( indexedTorrent );
	    });

	    if(bulkOp.length){
	        return bulkOp.execute();
	    }else{
	        return Promise.resolve([]);
	    }
	}
}


mongoose.model('Game', GameSchema);
