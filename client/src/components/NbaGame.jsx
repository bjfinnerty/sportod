import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import NbaTeam from './NbaTeam';
import {FeaturedGame} from './FeaturedGame';

import {connect} from 'react-redux';
import * as actionCreators from '../action_creators';


export const Game = React.createClass({

	mixins : [PureRenderMixin],

	getGames: function() {
	    return this.props.games || [];
	},

	getTeam: function(game, team){
		return game[team];
	},

	getFeaturedGame: function(){
		return this.props.games[this.props.meta.featuredIndex];
	},

	featureThis: function(featuredIndex){
		return this.props.changeFeatured(featuredIndex);
	},

	componentDidUpdate: function(){
		
		if(!this.props.meta.loaded){
			this.props.loadTodaysGames(this.props.meta.date);
		}
	},

	loadGames: function(date){
		this.props.refreshGames(date);
	},	

	render: function() {

		console.log('render');
		let games = this.getGames().map( (game, idx) =>
				<div key={game._id} className='game' onClick={()=> {this.featureThis(idx)}}>
			        <NbaTeam team={this.getTeam(game, 'home_team')} align='left '/>
			        <div> VS </div>
			        <NbaTeam team={this.getTeam(game, 'away_team')} align='right'/>
			    </div>
		    )
		if(games.length){
			return <div className='games'>
				<FeaturedGame game={this.getFeaturedGame()}/>
				{games}
			</div>;
		}else{
			return <div className='games'>
				No games loaded for this day
				<button onClick={(e)=> this.loadGames(this.props.meta.date)}> Reload Game Data </button>
			</div>;
		}
		
	}

});

// 			        //<Torrents gameId={game.game_id} torrents={game.torrents} {...this.props} ></Torrents>



function mapStateToProps(state){
	return{
		'games' : state.get('games').toJS(),
		'loaded':state.get('loaded'),
		'loading':state.get('loading'),
		'meta' : state.get('meta').toJS()
	}
}

export const NbaGame = connect(mapStateToProps, actionCreators)(Game);