var masterKat = require('../utils/kat.js'),
	moment = require('moment'),
	_ = require('lodash');

var defaultOpts = {	
	q		: 'nba',
	field	: 'time_add',
	sorder	: 'desc',
	page	: 1
}

var filterList = {
	//definition 	: ['720p', '1080p'],
	//frameRate 	: '60fps',
	size		: 2000000000,
	seeds		: 0,
	verified	: 0
}	

var getQueryString = function(game, teams) {

	var query = ['nba'];
	var gameDate = moment(game.game_date_est, 'YYYY-MM-DDT00:00:00');

	teams.forEach(function(team, i){
		query.push(team.team_name);
	});

	//query.push(gameDate.date(), gameDate.month() + 1, gameDate.year());
	query.push(gameDate.year());

	defaultOpts.q = query.join(' ');

	return defaultOpts;

}


var filterResults = function(torrentList, date){
	
	var firstPass = masterKat.filterResults(torrentList, filterList);

	var validTorrents = _.reject(firstPass, function(torrent){
		// Check for the definition in the title
		if(torrent.name.indexOf('720') === -1 && torrent.name.indexOf('1080') === -1){
			return false;
		}else if(moment(new Date(torrent.pubDate)) > moment(date) && moment(new Date(torrent.pubDate)) < moment(date).add(3, 'days')){
			// If the torrent file was uploaded before the date the game was played or more than 3 days later, reject it. It is the wrong game
			return false;
		}else{
			return true;
		}
		
	});

	return validTorrents
};

module.exports = {
	getQueryString: getQueryString,
	searchKat : masterKat.searchKat,
	parseResults: masterKat.parseResults,
	filterResults: filterResults
}