var fs = require("fs");
var path = require("path"),
  mongoose = require('mongoose'),
  Team = mongoose.model('Team');

var load = function(){
    var filePath = path.join(__dirname, "../data/teams.json");

    fs.readFile(filePath, 'utf8', function (err, data) {

        if (err) throw err;
        var teams = JSON.parse(data);

        Team.collection.insert(teams, function(err, docs){
            if (err) {
                return error;
            } else {
                return docs;
            }
        });
    });
};

module.exports.loadTeams = load;

