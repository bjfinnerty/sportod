var request = require('request-promise'),
    Promise = require('bluebird'),
    _ = require('lodash');


// NBA Season Constants
var katUrlRoot 	= 'https://kat.cr/json.php';


var searchKat = function(options) {
	
	var queryOptions = _.extend(options, {});

	return request.get({
        uri: katUrlRoot,
        qs : queryOptions
    });

};

var parseResults = function(data){
	var jsonData = JSON.parse(data);
	return jsonData.list;
}

var filterResults = function(torrentList, filterList){

	var filteredTorrents = [];
				 
	torrentList.forEach(function(torrent, i){

		var stillValid = true;
		_.each(filterList, function(val, key){
			if(stillValid && torrent[key] < val){
				stillValid = false;
			}
		});
		if(stillValid){
			filteredTorrents.push(torrent);
		}
	});

	return filteredTorrents;
}
    

module.exports = {
	searchKat: searchKat,
	parseResults: parseResults,
	filterResults: filterResults
}
